from .test_data import test_van_data, test_clients, test_weights
from recommendation_model.model import RecomendationModel


def predict_seat(van_id, user_id):
    coupe = get_coupe(van_id)
    user_properties = get_user_properties(user_id)
    user_weights = get_user_weights(user_id)

    return RecomendationModel('manhattan').predict(user_properties, user_weights, coupe)

def get_coupe(van_id):
    # Возвращает место в вагоне с критериями пользователей, которые находятся на этих местах (seat, у которых есть tecket_id)
    return test_van_data

def get_user_properties(user_id):
    # Возвращает критерии пользователя
    return test_clients[0]

def get_user_weights(user_id):
    # Возращает веса критериев пользователя
    return test_weights