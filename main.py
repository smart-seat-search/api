from typing import Union
from model import core
from model.database import engine
from fastapi import FastAPI

from routers.users import router as users_router
from routers.seats import router as seats_router

core.Base.metadata.create_all(bind=engine)

app = FastAPI()

print("app init")

app.include_router (users_router)
app.include_router (seats_router)