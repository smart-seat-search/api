from pydantic import BaseModel
from typing import Optional

class PropertyDto(BaseModel):
    smoking: Optional[str] = "нейтрально"
    smokingWeight: Optional[float] = 0.5
    tableUsage: Optional[str] = "неважно"
    tableUsageWeight: Optional[float] = 0.5
    child: Optional[str] = "нейтрально"
    childWeight: Optional[float] = 0.5
    temperature: Optional[str] = "неважно"
    temperatureWeight: Optional[float] = 0.5
    leisure: Optional[str] = ""
    leisureWeight: Optional[float] = 0.5
    bagsNumber: Optional[int] = 1
    bagsNumberWeight: Optional[float] = 0.5
    sleepType: Optional[str] = "неважно"
    sleepTypeWeight: Optional[float] = 0.5

class CreateUserDto(BaseModel):
    fullName: str
    age: int
    sex: str

class TrainDto(BaseModel):
    trainId: int
    fromPlace: str
    wherePlace: str
    startDate: int
    endDate: int

class SearchInfoDto(BaseModel):
    train_id: int
    van_id: int
    user_id: int