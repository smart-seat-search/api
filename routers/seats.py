from fastapi import APIRouter
from routers.dto import TrainDto, SearchInfoDto
from controllers.property import predict_seat

router = APIRouter(prefix="/seats")

# Вывод информации о все местах в вагоне
@router.post("/")
def seats_get(train: TrainDto):
    return {
        "seats": "информация о местах"
    }

# Подбор лучшего места в вагоне
@router.post("/smart")
def smart_seats_get(info: SearchInfoDto):
    return predict_seat(info.van_id, info.user_id)