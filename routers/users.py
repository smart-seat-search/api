from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from model.database import get_db
from routers.dto import PropertyDto, CreateUserDto

router = APIRouter(prefix="/users")

# Получение пользователя
@router.get("/")
def getUser(q: int):
    return {
        "id": 228,
        "fullName": "Даниил Усков",
        "sex": "M",
        "age": 21,
        "property": {
            "smoking": "нет",
            "tableUsage": "часто",
            "child": "нейтрально",
            "temperature": "холодно",
            "leisure": "чтение книги",
            "bagsNumber": 2,
            "sleepType": "сова"
        }
    }

# Создание пользователя
@router.post("/")
def create_user(user: CreateUserDto):
    return {
         "status": "OK"
    }

# Загрузка свойств пользователя
@router.post("/propery/{user_id}")
def upload_property(user_id: int, property: PropertyDto, db: Session = Depends(get_db)):
    return {
        "status": "OK"
    }

# Удаление пользователя
@router.delete("/")
def userDelete(q: int):
    return {
        "status": "OK"
    }
