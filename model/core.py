from .database import Base
from sqlalchemy import Column, Integer, String, Boolean

class Property(Base):
    __tablename__ = "properties"

    id = Column(Integer, primary_key=True, index=True)

    smoking = Column(Boolean)
    smokingWeight = Column(Integer)
    tableUsage = Column(String)
    tableUsageWeight = Column(Integer)
    child = Column(String)
    childWeight = Column(Integer)
    temperature = Column(String)
    temperatureWeight = Column(Integer)
    bagsNumber = Column(Integer)
    bagsNumberWeight = Column(Integer)
    sleepType = Column(String)
    sleepTypeWeight = Column(Integer)

# class User(Base)
# class Ticket(Base)
# class Seat(Base)
# class Trip(Base)
# class Train(Base)
# class Van(Base)
