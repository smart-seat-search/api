from pydantic import BaseModel

class PropertyBase(BaseModel):
    smoking: bool
    smokingWeight: int
    tableUsage: str
    tableUsageWeight: int
    child: str
    childWeight: int
    temperature: str
    temperatureWeight: int
    bagsNumber: int
    bagsNumberWeight: int
    sleepType: str
    sleepTypeWeight: int

# class UserBase(BaseModel)
# class TicketBase(BaseModel)
# class SeatBase(BaseModel)
# class TripBase(BaseModel)
# class TrainBase(BaseModel)
# class VanBase(BaseModel)
